using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathTrigger : MonoBehaviour
{
    [SerializeField] private GameObject explodeEffect;
    [SerializeField] private GameObject playerBall;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("DeathTrigger"))
        {
            ExplodeStart();
            StartCoroutine(restartSceneTimer());
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("DeathTrigger"))
        {
            ExplodeStart();
            StartCoroutine(restartSceneTimer());
        }
    }

    private void ExplodeStart()
    {
        explodeEffect.SetActive(true);
        Destroy(playerBall);
    }

    private IEnumerator restartSceneTimer()
    {
        var part = explodeEffect.GetComponent<ParticleSystem>();
        yield return new WaitForSeconds(part.main.duration);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
