using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NearbyButtonTextController : MonoBehaviour
{
    [SerializeField] private float radius;
    [SerializeField] private LayerMask layerMask;
    [SerializeField] private GameObject actionButtonTextPanel;
    public bool isOpenDoor = false;
    public bool isNearby = false;

    void Update()
    {
        CheckPlayerAroundBladesButton();
    }

    private void CheckPlayerAroundBladesButton()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, radius, layerMask);
        bool isPlayer = false;
        foreach (Collider col in colliders)
        {
            if (col.CompareTag("Player"))
            {
                isPlayer = true;
            }

        }

        if (isPlayer)
        {
            isNearby = true;
            if (!isOpenDoor)
                actionButtonTextPanel.SetActive(true);
            else
                actionButtonTextPanel.SetActive(false);
        }
        else
        {
            isNearby = false;
            actionButtonTextPanel.SetActive(false);
        }
    }
}
