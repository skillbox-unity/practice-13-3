namespace Game.Inputs
{
    public class GlobalVars
    {
        #region InputVars

        public const string HORIZONTAL_AXIS = "Horizontal";
        public const string VERTICAL_AXIS = "Vertical";
        public const string JUMP_BUTTON = "Jump";

        #endregion

        public const int MAX_LEVEL = 5;
    }
}
